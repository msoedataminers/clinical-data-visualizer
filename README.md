﻿## Clinical Data Visualizer

This application takes queries from clinical researchers and displays patient encounter data from a web api call to a `Solr` web application created by Dr. Urbain.

The data displayed will allow researchers to identify trends in the data quickly instead of having to manually review a large dataset to which trends are not immediately evident.

### Technology Stack

#### Back End

The server runs on the `dotnet core 2.0` runtime using an `ASP.NET WebApi`.

#### Front End

The front end client is a `React` single-page application using a `Redux` data store also using `ReCharts` to display the graphs.

There are two input fields to which the researcher can fill out a query:

1. A search engine-like query bar that will use natural language processing to determine the dataset to return.

2. An advanced search function that will have populatable fields based on predefined subsets that can be combined to make a more detailed end dataset.

### How to Run

Download [dotnet core 2.0](https://www.microsoft.com/net/download/core)

Download [Node.js](https://nodejs.org/en/download/)

Open a command prompt, navigate to the `ClinicalDataVisualizer` directory, and run these commands:

```
dotnet restore
dotnet build
dotnet run 
```

The web service should now run on the port specified.
