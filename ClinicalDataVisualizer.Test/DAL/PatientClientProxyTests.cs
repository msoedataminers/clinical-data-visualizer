using System;
using System.Collections.Generic;
using ClinicalDataVisualizer.DAL;
using ClinicalDataVisualizer.DAL.Models;
using ClinicalDataVisualizer.Domain.Models;
using Moq;
using RestSharp;
using Xunit;

namespace ClinicalDataVisualizer.Test.DAL
{
    public class PatientClientProxyTests
    {
        private IPatientClientProxy _target;
        private Mock<IRestClientWrapper> _mockRestClientWrapper;

        [Fact]
        public void PatientClientProxyWhenNullRestClientWrapperThrows()
        {
            var exception = Record.Exception(() => _target = new PatientClientProxy(null));

            Assert.IsType<ArgumentNullException>(exception);
            Assert.Equal(((ArgumentNullException)exception).ParamName, "restClientWrapper");
        }

        [Fact]
        public void MakePatientCallWhenNullRequestThrows()
        {
            _mockRestClientWrapper = new Mock<IRestClientWrapper>();

            _target = new PatientClientProxy(_mockRestClientWrapper.Object);

            var exception = Record.Exception(() => _target.MakePatientCall(null));

            Assert.IsType<ArgumentNullException>(exception);
            Assert.Equal(((ArgumentNullException)exception).ParamName, "request");
        }

        [Fact]
        public void FetchEncountersWhenValidEncounterRequestReturnsEncounters()
        {
            _mockRestClientWrapper = new Mock<IRestClientWrapper>();

            _target = new PatientClientProxy(_mockRestClientWrapper.Object);

            var response = new PatientServiceResponse
            {
                Response = new ResponseDetails
                {
                    Docs = new List<Doc>
                    {
                        new Doc(),
                        new Doc()
                    }
                }
            };

            _mockRestClientWrapper.Setup(_ => _.PatientCall(It.IsAny<RestRequest>())).Returns(response);

            _target = new PatientClientProxy(_mockRestClientWrapper.Object);

            var patientServiceResponse = _target.MakePatientCall(new EncounterRequest());

            Assert.Equal(response, patientServiceResponse);
        }
    }
}
