using System;
using ClinicalDataVisualizer.DAL.Models;
using RestSharp;
using RestSharp.Deserializers;
using Xunit;

namespace ClinicalDataVisualizer.Test.DAL.Models
{
    public class PatientServiceResponseTests
    {
        [Fact]
        public void PatientServiceResponseDeserializesIntoObject()
        {
            var json = CreatePatientServiceResponseJson();

            var response = new RestResponse
            {
                Content = json
            };

            var jsonDeserializer = new JsonDeserializer();

            var patientResponse = jsonDeserializer.Deserialize<PatientServiceResponse>(response);

            Assert.Equal("24750679_1582352500_2011-07-23 00:00:00", patientResponse.Response.Docs[0].Id);
            Assert.Equal("encounter1", patientResponse.Response.Docs[0].EncounterNumber);
            Assert.Equal("patient1", patientResponse.Response.Docs[0].PatientNumber);
            Assert.Equal(Convert.ToDateTime("2014-03-29T00:00:00"), patientResponse.Response.Docs[0].StartDate);
            Assert.Equal(Convert.ToDateTime("2014-03-29T00:00:00"), patientResponse.Response.Docs[0].EndTime[0]);
            Assert.Equal(Convert.ToDateTime("1983-07-12T00:00:00"), patientResponse.Response.Docs[0].BirthDate);
            Assert.Equal("N", patientResponse.Response.Docs[0].VitalStatus);
            Assert.Equal("M", patientResponse.Response.Docs[0].Sex);
            Assert.Equal(10, patientResponse.Response.Docs[0].Age);
            Assert.Equal("Asian", patientResponse.Response.Docs[0].Race);
            Assert.Equal("Married", patientResponse.Response.Docs[0].MaritalStatus);
            Assert.Equal("54603", patientResponse.Response.Docs[0].Zip);
            Assert.Equal("test_text", patientResponse.Response.Docs[0].Note[0]);
            Assert.Equal("V49.89", patientResponse.Response.Docs[0].Icd9s[2]);
            Assert.Equal("10-19", patientResponse.Response.Docs[0].AgeCategory);
        }

        public string CreatePatientServiceResponseJson()
        {
            return "{\"response\": { \"docs\": [ { " +
                   "\"id\": \"24750679_1582352500_2011-07-23 00:00:00\", " +
                   "\"encounter_num\": \"encounter1\", " +
                   "\"patient_num\": \"patient1\", " +
                   "\"start_date\": \"2014-03-29T00:00:00Z\", " +
                   "\"end_date\": [\"2014-03-29T00:00:00Z\", \"2014-03-29T00:00:00Z\"], " +
                   "\"birth_date\": \"1983-07-12T00:00:00Z\", " +
                   "\"vital_status\": \"N\", " +
                   "\"sex\": \"M\", " +
                   "\"age\": \"10\", " +
                   "\"race\": \"Asian\", " +
                   "\"marital_status\": \"Married\", " +
                   "\"zip\": \"54603\", " +
                   "\"note_text\": [\"test_text\"], " +
                   "\"icd9s\": [\"246.9\", \"314.01\", \"V49.89\"], " +
                   "\"age_cat\": \"10-19\"} ] } }";
        }
    }
}