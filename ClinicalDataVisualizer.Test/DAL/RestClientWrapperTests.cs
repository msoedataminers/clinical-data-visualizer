using System;
using System.Collections.Generic;
using ClinicalDataVisualizer.DAL;
using ClinicalDataVisualizer.DAL.Models;
using Moq;
using RestSharp;
using RestSharp.Deserializers;
using Xunit;

namespace ClinicalDataVisualizer.Test.DAL
{
    public class RestClientWrapperTests
    {
        private IRestClientWrapper _target;
        private Mock<IRestClient> _mockRestClient;
        private Mock<IDeserializer> _mockJsonDeserializer;

        [Fact]
        public void PatientCallWhenNullRequestThrows()
        {
            _target = new RestClientWrapper();

            var exception = Record.Exception(() => _target.PatientCall(null));

            Assert.IsType<ArgumentNullException>(exception);
            Assert.Equal(((ArgumentNullException)exception).ParamName, "request");
        }

        [Fact]
        public void PatientCallWhenRestClientReturnsBadDataThenResponseDeserializesToNull()
        {
            _mockRestClient = new Mock<IRestClient>();

            _mockRestClient.Setup(_ => _.Execute(It.IsAny<RestRequest>())).Returns(new RestResponse
            {
                Content = "{ }"
            });

            _target = new RestClientWrapper
            {
                PatientClient = _mockRestClient.Object
            };

            var patientCallResponse = _target.PatientCall(new RestRequest());

            Assert.Null(patientCallResponse.Response);
        }

        [Fact]
        public void PatientCallWhenValidRequestReturnsPatientServiceResponse()
        {
            _mockRestClient = new Mock<IRestClient>();
            _mockJsonDeserializer = new Mock<IDeserializer>();

            _target = new RestClientWrapper();

            var response = new PatientServiceResponse
            {
                Response = new ResponseDetails
                {
                    Docs = new List<Doc>
                    {
                        new Doc(),
                        new Doc()
                    }
                }
            };

            _mockRestClient.Setup(_ => _.Execute(It.IsAny<RestRequest>())).Returns(new RestResponse());

            _mockJsonDeserializer.Setup(_ => _.Deserialize<PatientServiceResponse>(It.IsAny<RestResponse>())).Returns(response);

            _target = new RestClientWrapper
            {
                PatientClient = _mockRestClient.Object,
                Deserializer = _mockJsonDeserializer.Object
            };

            var patientServiceResponse = _target.PatientCall(new RestRequest());

            Assert.Equal(response, patientServiceResponse);
        }
    }
}
