using System;
using System.Collections.Generic;
using AutoMapper;
using ClinicalDataVisualizer.Automapper;
using ClinicalDataVisualizer.DAL.Models;
using ClinicalDataVisualizer.Domain.Models;
using Xunit;

namespace ClinicalDataVisualizer.Test.Automapper
{
    public class AutomapperFactoryTests
    {
        private IMapper _target;

        [Fact]
        public void TestMapperConfigurationIsValid()
        {
            _target = AutomapperFactory.CreateMapper();

            _target.ConfigurationProvider.AssertConfigurationIsValid();
        }

        [Fact]
        public void TestMappingFromDocToEncounter()
        {
            _target = AutomapperFactory.CreateMapper();

            var doc = new Doc
            {
                Id = "Hello world",
                EncounterNumber = "123456789",
                PatientNumber = "987654321",
                StartDate = DateTime.Today,
                EndTime = new List<DateTime>
                {
                    DateTime.Today.AddDays(1),
                    DateTime.Today.AddDays(2)
                },
                BirthDate = DateTime.MinValue,
                VitalStatus = "Good",
                Sex = "M",
                Age = 12,
                Race = "Caucasian",
                Zip = "53202",
                Note = new List<string>
                {
                    "This person is very healthy",
                    "I'm serious"
                },
                Icd9s = new List<string>
                {
                    "123",
                    "456",
                    "789"
                },
                AgeCategory = "10-20"
            };

            var encounter = _target.Map<Encounter>(doc);

            Assert.Equal(doc.Id, encounter.Id);
            Assert.Equal(doc.EncounterNumber, encounter.EncounterNumber.ToString());
            Assert.Equal(doc.PatientNumber, encounter.PatientNumber.ToString());
            Assert.Equal(doc.StartDate, encounter.StartDate);
            Assert.Equal(doc.EndTime[0], encounter.EndDate);
            Assert.Equal(doc.BirthDate, encounter.BirthDate);
            Assert.Equal(doc.VitalStatus, encounter.Vitals);
            Assert.Equal(doc.Sex, encounter.Sex);
            Assert.Equal(doc.Age, encounter.Age);
            Assert.Equal(doc.Zip, encounter.Zip);
            Assert.Equal(string.Join("\n", doc.Note), encounter.Note);
            Assert.Equal(doc.Icd9s.Count, encounter.Icd9Codes.Count);
            Assert.Equal(doc.AgeCategory, encounter.AgeCategory);
        }
    }
}
