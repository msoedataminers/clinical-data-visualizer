using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ClinicalDataVisualizer.DAL;
using ClinicalDataVisualizer.DAL.Models;
using ClinicalDataVisualizer.Domain;
using ClinicalDataVisualizer.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace ClinicalDataVisualizer.Test.Domain
{
    public class ParametersFactoryTests
    {
        private IParametersFactory _target;
        private ConfigurationProxy _mockConfigurationProxy;
        private Mock<IOptions<ConfigurationProxy>> _mockOptions;
        private DataContext _mockDataContext;
        private Mock<IMapper> _mockMapper;

        [Fact]
        public void ParametersFactoryWhenNullConfigurationProxyThrows()
        {
            _mockOptions = new Mock<IOptions<ConfigurationProxy>>();
            _mockOptions.Setup(_ => _.Value).Returns((ConfigurationProxy)null);

            var exception = Record.Exception(() => _target = new ParametersFactory(_mockOptions.Object, null, null));

            Assert.IsType<ArgumentNullException>(exception);
            Assert.Equal(((ArgumentNullException)exception).ParamName, "configurationProxy");
        }

        [Fact]
        public void ParametersFactoryWhenNullDataContextThrows()
        {
            _mockOptions = new Mock<IOptions<ConfigurationProxy>>();
            _mockOptions.Setup(_ => _.Value).Returns(new ConfigurationProxy());

            var exception = Record.Exception(() => _target = new ParametersFactory(_mockOptions.Object, null, null));

            Assert.IsType<ArgumentNullException>(exception);
            Assert.Equal(((ArgumentNullException)exception).ParamName, "context");
        }

        [Fact]
        public void ParametersFactoryWhenNullMapperThrows()
        {
            _mockOptions = new Mock<IOptions<ConfigurationProxy>>();
            _mockOptions.Setup(_ => _.Value).Returns(new ConfigurationProxy());
            _mockDataContext = CreateMockDataContext();

            var exception = Record.Exception(() => _target = new ParametersFactory(_mockOptions.Object, _mockDataContext, null));

            Assert.IsType<ArgumentNullException>(exception);
            Assert.Equal(((ArgumentNullException)exception).ParamName, "mapper");
        }

        [Fact]
        public void CreateQueryParametersWhenSettingsExistReturnsParameters()
        {
            _mockOptions = new Mock<IOptions<ConfigurationProxy>>();
            _mockDataContext = CreateMockDataContext();
            _mockMapper = new Mock<IMapper>();

            _mockOptions.Setup(_ => _.Value).Returns(new ConfigurationProxy());
            _mockMapper.Setup(_ => _.Map<QueryParameter>(It.IsAny<QueryEntity>()))
                .Returns<QueryEntity>(_ => new QueryParameter
                {
                    Query = _.Query,
                    Operators = _.Type.Operators.Select(op => op.Operator.Operator).ToList()
                });

            _target = new ParametersFactory(_mockOptions.Object, _mockDataContext, _mockMapper.Object);

            var queryParameters = _target.CreateQueryParameters();

            Assert.Equal(queryParameters.Count, _mockDataContext.QueryParameters.Count());

            foreach (var query in queryParameters)
            {
                Assert.Equal(query.Operators.Count, _mockDataContext.Operators.Count());
            }
        }

        [Fact]
        public void CreateOperatorsWhenSettingsExistReturnsParameters()
        {
            _mockConfigurationProxy = new ConfigurationProxy
            {
                Operators = new List<string>
                {
                    "=",
                    ">"
                }
            };

            _mockOptions = new Mock<IOptions<ConfigurationProxy>>();
            _mockDataContext = CreateMockDataContext();
            _mockMapper = new Mock<IMapper>();

            _mockOptions.Setup(_ => _.Value).Returns(_mockConfigurationProxy);

            _target = new ParametersFactory(_mockOptions.Object, _mockDataContext, _mockMapper.Object);

            var operators = _target.CreateOperators();

            Assert.Equal(operators.Count, _mockConfigurationProxy.Operators.Count);

            Assert.Equal(operators[0].Name, _mockConfigurationProxy.Operators[0]);
            Assert.Equal(operators[0].Value, _mockConfigurationProxy.Operators[0]);

            Assert.Equal(operators[1].Name, _mockConfigurationProxy.Operators[1]);
            Assert.Equal(operators[1].Value, _mockConfigurationProxy.Operators[1]);
        }

        [Fact]
        public void CreateFilterParametersWhenSettingsExistReturnsParameters()
        {
            _mockConfigurationProxy = new ConfigurationProxy
            {
                FilterParameters = new List<string>
                {
                    "Hello",
                    "World"
                }
            };

            _mockOptions = new Mock<IOptions<ConfigurationProxy>>();
            _mockDataContext = CreateMockDataContext();
            _mockMapper = new Mock<IMapper>();

            _mockOptions.Setup(_ => _.Value).Returns(_mockConfigurationProxy);

            _target = new ParametersFactory(_mockOptions.Object, _mockDataContext, _mockMapper.Object);

            var filterParameters = _target.CreateFilterParameters();

            Assert.Equal(filterParameters.Count, _mockConfigurationProxy.FilterParameters.Count);

            Assert.Equal(filterParameters[0].Name, _mockConfigurationProxy.FilterParameters[0]);
            Assert.Equal(filterParameters[0].Value, _mockConfigurationProxy.FilterParameters[0].ToLower());

            Assert.Equal(filterParameters[1].Name, _mockConfigurationProxy.FilterParameters[1]);
            Assert.Equal(filterParameters[1].Value, _mockConfigurationProxy.FilterParameters[1].ToLower());
        }

        private static DataContext CreateMockDataContext()
        {
            var builder = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString());

            var context = new DataContext(builder.Options);

            context.Operators.Add(new OperatorEntity
            {
                Id = 1,
                Operator = "="
            });
            context.Operators.Add(new OperatorEntity
            {
                Id = 2,
                Operator = ">"
            });

            context.Types.Add(new TypeEntity
            {
                Id = 1,
                Type = "number"
            });

            context.TypeOperators.Add(new TypeOperatorEntity
            {
                TypeId = 1,
                OperatorId = 1
            });
            context.TypeOperators.Add(new TypeOperatorEntity
            {
                TypeId = 1,
                OperatorId = 2
            });

            context.QueryParameters.Add(new QueryEntity
            {
                Id = 1,
                Query = "Age",
                TypeId = 1
            });
            context.QueryParameters.Add(new QueryEntity
            {
                Id = 2,
                Query = "Sex",
                TypeId = 1
            });

            context.SaveChanges();

            return context;
        }
    }
}
