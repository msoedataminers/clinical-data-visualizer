using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ClinicalDataVisualizer.DAL;
using ClinicalDataVisualizer.DAL.Models;
using ClinicalDataVisualizer.Domain;
using ClinicalDataVisualizer.Domain.Models;
using Moq;
using Xunit;

namespace ClinicalDataVisualizer.Test.Domain
{
    public class EncounterFactoryTests
    {
        private IEncounterFactory _target;
        private Mock<IPatientClientProxy> _mockPatientClientProxy;
        private Mock<IMapper> _mockMapper;

        [Fact]
        public void DataFactoryWhenNullPatientClientProxyThrows()
        {
            var exception = Record.Exception(() => _target = new EncounterFactory(null, null));

            Assert.IsType<ArgumentNullException>(exception);
            Assert.Equal(((ArgumentNullException)exception).ParamName, "patientClientProxy");
        }

        [Fact]
        public void DataFactoryWhenNullMapperThrows()
        {
            _mockPatientClientProxy = new Mock<IPatientClientProxy>();

            var exception = Record.Exception(() => _target = new EncounterFactory(_mockPatientClientProxy.Object, null));

            Assert.IsType<ArgumentNullException>(exception);
            Assert.Equal(((ArgumentNullException)exception).ParamName, "mapper");
        }

        [Fact]
        public void FetchEncountersWhenNullQueryThrows()
        {
            _mockPatientClientProxy = new Mock<IPatientClientProxy>();
            _mockMapper = new Mock<IMapper>();

            _target = new EncounterFactory(_mockPatientClientProxy.Object, _mockMapper.Object);

            var exception = Record.Exception(() => _target.FetchEncounters(null));

            Assert.IsType<ArgumentNullException>(exception);
            Assert.Equal(((ArgumentNullException)exception).ParamName, "query");
        }

        [Fact]
        public void FetchEncountersWhenValidEncounterRequestReturnsEncounters()
        {
            _mockPatientClientProxy = new Mock<IPatientClientProxy>();
            _mockMapper = new Mock<IMapper>();

            var response = new PatientServiceResponse
            {
                Response = new ResponseDetails
                {
                    Docs = new List<Doc>
                    {
                        new Doc(),
                        new Doc()
                    }
                }
            };

            _mockPatientClientProxy.Setup(_ => _.MakePatientCall(It.IsAny<EncounterRequest>())).Returns(response);

            _mockMapper.Setup(_ => _.Map<Encounter>(It.IsAny<Doc>())).Returns(new Encounter());

            _target = new EncounterFactory(_mockPatientClientProxy.Object, _mockMapper.Object);

            var encounters = _target.FetchEncounters(new EncounterRequest());

            Assert.Equal(response.Response.Docs.Count, encounters.Count());
        }
    }
}
