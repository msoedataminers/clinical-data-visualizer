const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const babelSettings = {
    presets: ['env', 'react'],
    plugins: [
        'transform-object-rest-spread'
    ]
};

module.exports = {
    entry: './app/index.js',
    output: {
        path: path.resolve('wwwroot'),
        filename: 'index.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: babelSettings
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('css-loader')
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './app/index.html',
            filename: 'index.html',
            inject: 'body',
            title: 'Production'
        }),
        new ExtractTextPlugin('/style.css')
    ]
}
