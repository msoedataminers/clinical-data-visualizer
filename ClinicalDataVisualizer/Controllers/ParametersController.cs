using System;
using System.Collections.Generic;
using ClinicalDataVisualizer.Domain;
using ClinicalDataVisualizer.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace ClinicalDataVisualizer.Controllers
{
    [Route("api/[controller]")]
    public class ParametersController : Controller
    {
        private readonly IParametersFactory _parametersFactory;

        public ParametersController(IParametersFactory parametersFactory)
        {
            _parametersFactory = parametersFactory ?? throw new ArgumentNullException(nameof(parametersFactory));
        }

        // GET api/parameters/query
        [HttpGet("query")]
        public IEnumerable<QueryParameter> RequestQueryParameters()
        {
            var queryParameters = _parametersFactory.CreateQueryParameters();

            return queryParameters;
        }

        // GET api/parameters/operators
        [HttpGet("operators")]
        public IEnumerable<Parameter> RequestOperators()
        {
            var operators = _parametersFactory.CreateOperators();

            return operators;
        }

        // GET api/parameters/filters
        [HttpGet("filters")]
        public IEnumerable<Parameter> RequestFilterParameters()
        {
            var filterParameters = _parametersFactory.CreateFilterParameters();

            return filterParameters;
        }
    }
}
