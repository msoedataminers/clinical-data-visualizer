using System;
using System.Collections.Generic;
using ClinicalDataVisualizer.Domain;
using ClinicalDataVisualizer.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace ClinicalDataVisualizer.Controllers
{
    [Route("api/[controller]")]
    public class EncounterController : Controller
    {
        private readonly IEncounterFactory _encounterFactory;

        public EncounterController(IEncounterFactory encounterFactory)
        {
            _encounterFactory = encounterFactory ?? throw new ArgumentNullException(nameof(encounterFactory));
        }

        // POST api/data
        [HttpPost]
        public IEnumerable<Encounter> RequestData([FromBody] EncounterRequest query)
        {
            var data = _encounterFactory.FetchEncounters(query);

            return data;
        }
    }
}
