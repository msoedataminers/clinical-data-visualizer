import sqlite3
import os

dir = os.path.dirname(__file__)

conn = sqlite3.connect('Database.db')
c = conn.cursor()

# create schemas
c.execute('''CREATE TABLE operator
                (operatorid integer primary key,
                operator string)''')
c.execute('''CREATE TABLE type
                (typeid integer primary key,
                name string)''')
c.execute('''CREATE TABLE typeoperator
                (typeid integer references type(typeid),
                operatorid integer references operator(operatorid),
                primary key(typeid, operatorid))''')
c.execute('''CREATE TABLE queries
                (id integer primary key,
                query string,
                typeid integer references type(typeid))''')

# import operators
operators = open(os.path.join(dir,'operators.csv')).read().split('\n')
for i in range(1, len(operators) + 1):
    c.execute('INSERT INTO operator VALUES (' + str(i) + ', \''+ operators[i - 1] + '\')')

# import types
types = open(os.path.join(dir, 'queryTypes.csv')).read().split('\n')
for i in range(1, len(types) + 1):
    querytype = types[i - 1].split(',')
    c.execute('INSERT INTO type VALUES (' + str(i) + ', \''+ querytype[0] + '\')')
    for j in range(1, len(querytype)):
        c.execute('INSERT INTO typeoperator VALUES (' + str(i) + ', \''+ querytype[j] + '\')')

# import queries
queries = open(os.path.join(dir, 'queries.csv')).read().split('\n')
for i in range(1, len(queries) + 1):
    query = queries[i - 1].split(',')
    c.execute('INSERT INTO queries VALUES (' + str(i) + ', \''+ query[0] + '\', \'' + query[1] + '\')')

conn.commit()
conn.close()