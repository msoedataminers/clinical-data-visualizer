using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ClinicalDataVisualizer.DAL.Models;
using ClinicalDataVisualizer.Domain.Models;

namespace ClinicalDataVisualizer.Automapper
{
    public class OperatorResolver : IValueResolver<QueryEntity, QueryParameter, IList<string>>
    {
        public IList<string> Resolve(QueryEntity source, QueryParameter destination, IList<string> destMember, ResolutionContext context)
        {
            return source.Type.Operators.Select(_ => _.Operator.Operator).ToList();
        }
    }
}