﻿using System.Linq;
using AutoMapper;
using ClinicalDataVisualizer.DAL.Models;
using ClinicalDataVisualizer.Domain.Models;

namespace ClinicalDataVisualizer.Automapper
{
    public static class AutomapperFactory
    {
        public static Mapper CreateMapper()
        {
            return new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Doc, Encounter>()
                    .ForMember(dest => dest.EndDate, opt => opt.MapFrom(_ => _.EndTime.First()))
                    .ForMember(dest => dest.Vitals, opt => opt.MapFrom(_ => _.VitalStatus))
                    .ForMember(dest => dest.Note, opt => opt.MapFrom(_ => string.Join("\n", _.Note)))
                    .ForMember(dest => dest.Icd9Codes, opt => opt.MapFrom(_ => _.Icd9s));

                cfg.CreateMap<QueryEntity, QueryParameter>()
                    .ForMember(dest => dest.Type, opt => opt.MapFrom(_ => _.Type.Type))
                    .ForMember(dest => dest.Operators, opt => opt.ResolveUsing<OperatorResolver>());
            }));
        }
    }
}
