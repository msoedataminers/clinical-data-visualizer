import React from 'react';
import { connect } from 'react-redux';
import { BarChart, Bar, XAxis, YAxis, Tooltip } from 'recharts';

import { deleteGraph } from '../actions/actions';

//
// Graph
// Expected props:
// id: int
// data: array
// deleteGraph: func
//
export class Graph extends React.PureComponent {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.deleteGraph(this.props.id);
    }

    render() {
        return (
            <span>
                <button onClick={this.handleClick}>X</button>
                <BarChart width={600} height={300} data={this.props.data}>
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Bar dataKey="value" />
                </BarChart>
            </span>
        );
    }
}

export default connect(null, { deleteGraph })(Graph);
