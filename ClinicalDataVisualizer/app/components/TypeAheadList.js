import React from 'react';

//
// TypeAheadList
// Expected Props:
// id: string
// options: array
// selectedValue: string
// onChange: func
//
const TypeAheadList = ({ id, options, selectedValue, onChange }) => {
    return (
        <div>
            <input list={id} onChange={onChange} className="form-control type-ahead" value={selectedValue} />
            <datalist id={id}>
                {
                    !options ? <option /> :
                    options.map((option, i) => {
                        return <option key={i} value={option} />;
                    })
                }
            </datalist>
        </div>
    );
}

export default TypeAheadList;
