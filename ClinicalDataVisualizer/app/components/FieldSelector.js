import React from 'react';
import { connect } from 'react-redux';

import { toggleFieldDisplay, showAllFields, hideAllFields } from '../actions/actions';

import { toTitleCase } from '../utils/utils';

//
// FieldSelector
// Expected Props
// fieldDisplay: object
// toggleFieldDisplay: func
// showAllFields: func
// hideAllFields: func
//
const FieldSelector = ({ fieldDisplay, toggleFieldDisplay, showAllFields, hideAllFields }) => {
    return (
        <div>
            <button className="btn" style={{ float: 'left' }} onClick={() => showAllFields()}>Show All</button>
            <button className="btn" onClick={() => hideAllFields()}>Hide All</button>
            {
                Object.keys(fieldDisplay).map((field, i) => {
                    const style = {
                        color: fieldDisplay[field] ? '' : '#d3d3d3'
                    };

                    return <div key={i} className="pointer" onClick={() => toggleFieldDisplay(field)} style={style}>{toTitleCase(field)}</div>;
                })
            }
        </div>
    );
}

const mapStateToProps = state => {
    const { fieldDisplay } = state.filter;

    return {
        fieldDisplay
    };
}

export default connect(mapStateToProps, { toggleFieldDisplay, showAllFields, hideAllFields })(FieldSelector);
