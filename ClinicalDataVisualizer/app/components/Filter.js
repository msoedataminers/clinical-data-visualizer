import React from 'react';
import { connect } from 'react-redux';

import Collapsible from 'react-collapsible';

import { addFilter, removeFilter, clearFilters, editCurrentFilter, fetchFilterParameters } from '../actions/actions';

import SelectList from './SelectList';

//
// Filter
// Expected props:
// filter: object
// addFilter: func
// removeFilter: func
// clearFilters: func
// editCurrentFilter: func
// fetchFilterParameters: func
//
export class Filter extends React.Component {
    componentWillMount() {
        if (!this.props.filter.filterParameters) {
            this.props.fetchFilterParameters();
        }
    }

    render() {
        const { addFilter, removeFilter, clearFilters, editCurrentFilter } = this.props;
        const { filters, operators, filterParameters, currentFilter } = this.props.filter;

        return (
            <div>
                <SelectList options={filterParameters} value={currentFilter.field}
                    onChange={ e => editCurrentFilter({ field: e.target.value, operator: currentFilter.operator, value: currentFilter.value }) } />
                &nbsp;
                <SelectList options={operators} value={currentFilter.operator}
                    onChange={ e => editCurrentFilter({ field: currentFilter.field, operator: e.target.value, value: currentFilter.value }) } />
                &nbsp;
                <input onChange={ e => editCurrentFilter({ field: currentFilter.field, operator: currentFilter.operator, value: e.target.value }) }
                    className="form-control small" value={currentFilter.value} />
                <button className="btn" style={{ marginLeft: '10px' }} onClick={ () => addFilter() }>Add Filter</button>
                <button className="btn" style={{ marginLeft: '10px' }} onClick={ () => clearFilters() }>Clear Filters</button>
                <Collapsible trigger="Click Me to see Filters" transitionTime={100} className="Collapsible">
                    {
                        filters.map((filter, id) => {
                            return <div key={id}>{filter.field} {filter.operator} {filter.value} <button onClick={() => removeFilter(id)}>X</button></div>;
                        })
                    }
                </Collapsible>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { filter } = state;

    return {
        filter
    };
}

export default connect(mapStateToProps, { addFilter, removeFilter, clearFilters, editCurrentFilter, fetchFilterParameters })(Filter);
