import React from 'react';
import { connect } from 'react-redux';

import { addRule, removeConditionalBlock } from '../actions/actions';

import Rule from './Rule';

//
// ConditionalBlock
// Expected props:
// id: int
// rules: array
// fields: array
// operators: array
// addRule: func
// removeConditionalBlock: func
//
export class ConditionalBlock extends React.Component {

    render() {
        const { id, rules, fields, operators, addRule, removeConditionalBlock } = this.props;

        return (
            <div>
                <div style={{ background: 'aliceBlue' }}>
                    <span>
                        <button className="btn" onClick={ () => addRule(id) }>Add Rule</button>
                        <button className="btn" style={{ marginLeft: '25px'}} onClick={ () => removeConditionalBlock(id) }>Remove Group</button>
                    </span>
                    <ul style={{ listStyleType: 'none' }}>
                        {
                            rules.map((rule, i) => {
                                return <li key={i} style={{ margin: '10px' }}>
                                    <Rule id={i} blockId={id} rule={rule} fields={fields} operators={operators} />
                                </li>;
                            })
                        }
                    </ul>
                </div>
                <br />
            </div>
        );
    }
}

export default connect(null, { addRule, removeConditionalBlock })(ConditionalBlock);
