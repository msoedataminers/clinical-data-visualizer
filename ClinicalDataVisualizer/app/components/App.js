import React from 'react';

import Search from './Search';
import Results from './Results';


export default () => {
    return [
        <Search key={1}/>,
        <hr key={2} style={{ width: '100%' }}/>,
        <Results key={3}/>
    ];
}
