import React from 'react';
import { checkSearchTermEquality } from '../utils/utils';

export default ({ cellValue, textQuery }) => {
    if (!cellValue || !textQuery) return cellValue;
    const arrayAtStart = Array.isArray(cellValue);
    if (arrayAtStart) {
        cellValue = cellValue.join(" ");
    }
    else if (typeof cellValue != "string") {
        cellValue = String(cellValue);
    }
    return <span>
        {
            cellValue.trim().split(/\s+/).map((resultWord, i) => {
                return textQuery.trim().split(/\s+/)
                    .some(queryWord => checkSearchTermEquality(resultWord, queryWord))
                    ? <strong key={i}>{resultWord}</strong>
                    : <span key={i}>{resultWord}</span>;
            }).reduce((prev, curr) => Array.isArray(prev)
                ? prev.concat(arrayAtStart ? ", " : " ", curr)
                : [prev, arrayAtStart ? ", " : " ", curr])
        }
    </span>;
}
