import React from 'react';
import { connect } from 'react-redux';

import { addConditionalBlock, fetchQueryParameters, fetchOperators } from '../actions/actions';

import ConditionalBlock from './ConditionalBlock';

//
// QueryBuilder
// Expected props:
// conditionalBlocks: array
// fields: array
// operators: array
// addConditionalBlock: func
// fetchQueryParameters: func
// fetchOperators: func
//
export class QueryBuilder extends React.Component {
    componentWillMount() {
        if (!this.props.fields) {
            this.props.fetchQueryParameters();
        }

        if (!this.props.operators) {
            this.props.fetchOperators();
        }
    }

    render() {
        const { conditionalBlocks, fields, operators, addConditionalBlock } = this.props;

        return (
            <div>
                <br />
                {
                    conditionalBlocks.map((conditionalBlock, i) =>
                        <ConditionalBlock key={i} id={i} rules={conditionalBlock.rules} fields={fields} operators={operators} />
                    )
                }
                <button className="btn" onClick={ () => addConditionalBlock() }>Add Group</button>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { advancedSearch } = state.query;
    const { queryParameters, operators } = state.filter;

    return {
        conditionalBlocks: advancedSearch,
        fields: queryParameters,
        operators
    }
}

export default connect(mapStateToProps, { addConditionalBlock, fetchQueryParameters, fetchOperators })(QueryBuilder);
