import React from 'react';
import { connect } from 'react-redux';

import { editRule, removeRule } from '../actions/actions';

import TypeAheadList from './TypeAheadList';
import SelectList from './SelectList';

//
// Rule
// Expected props:
// id: int
// blockId: int
// rule: object
// fields: array
// operators: array
// editRule: func
// removeRule: func
//
export class Rule extends React.Component {
    componentWillMount() {
        const { editRule, blockId, id, fields, operators } = this.props;

        editRule(blockId, id, { field: '', operator: operators[0].value, value: '' });
    }

    onFieldChange(e) {
        const { rule, blockId, id, fields, editRule } = this.props;

        let fieldFound = false;
        for (let i in fields) {
            if (fields[i].query === e.target.value) {
                fieldFound = true;
                editRule(blockId, id, { 
                    ...rule,
                    field: e.target.value,
                    type: fields[i].type,
                    availableOperators: fields[i].operators.map(_ => ({ name: _, value: _ }))
                });
            }
        }

        if (!fieldFound) editRule(blockId, id, { ...rule, field: e.target.value, type: null, availableOperators: null });
    }

    render() {
        const { fields, operators, blockId, id, rule, editRule, removeRule } = this.props;

        return (
            <div>
                <TypeAheadList id={`${blockId}-${id}`} options={fields.map(_ => _.query)} selectedValue={rule.field}
                    onChange={this.onFieldChange.bind(this)} />
                <SelectList options={rule.availableOperators ? rule.availableOperators : operators} value={rule.operator}
                    onChange={ e => editRule(blockId, id, { ...rule, operator: e.target.value }) } />
                <input type={rule.type ? rule.type : 'text'} onChange={ e => editRule(blockId, id, { ...rule, value: e.target.value }) }
                    className="form-control small" value={rule.value} />
                <button className="btn" style={{ marginLeft: '10px' }} onClick={ () => removeRule(blockId, id) }>Remove Rule</button>
            </div>
        );
    }
}

export default connect(null, { editRule, removeRule })(Rule);
