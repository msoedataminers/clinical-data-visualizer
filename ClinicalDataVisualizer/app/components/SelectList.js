import React from 'react';

//
// SelectList
// Expected Props:
// options: array
// value: string
// onChange: func
//
const SelectList = ({ options, value, onChange }) => {
    return (
        <select onChange={onChange} value={value} className="form-control">
            {
                !options ? <option /> :
                options.map((option, i) => {
                    return <option key={i} value={option.value}>{option.name}</option>;
                })
            }
        </select>
    );
}

export default SelectList;
