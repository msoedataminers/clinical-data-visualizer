import React from 'react';
import { connect } from 'react-redux';
import ReactTable from "react-table";
import SkyLight from 'react-skylight';
import "react-table/react-table.css";

import { addGraph, setNote } from '../actions/actions';

import { createGraph, toTitleCase, checkSearchTermEquality } from '../utils/utils';

import Graph from './Graph';
import Filter from './Filter';
import CellText from './CellText';
import FieldSelector from './FieldSelector';
import ResultData from './ResultData';

import { filterEncounters, filterDisplayedFields } from '../selectors';

//
// Results
// Expected props:
// addGraph: func
// setNote: func
// deleteGraph: func
//
export class Results extends React.Component {
    constructor(props) {
        super(props);
        this.state = { graphField: 'age' };

        this.onAddGraph = this.onAddGraph.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    onAddGraph() {
        const graph = createGraph(this.props.patients, this.state.graphField);

        this.props.addGraph(graph);
    }

    handleChange(event) {
        this.setState({ graphField: event.target.value });
    }

    render() {
        const { shouldDisplayResults, filterParameters, patients, graphs, textQuery, rawPatientData } = this.props;

        if (shouldDisplayResults) {
            return (
                <div className="container">
                    <Filter />
                    <br/>
                    <div>
                        <div className="col-md-2">
                            <FieldSelector />
                        </div>
                        <div className="col-md-10">
                            <ResultData patients={rawPatientData} />
                            <ReactTable data={patients} getTdProps={(state, rowInfo, column, instance) => {
                                    return {
                                        onClick: (e, handleOriginal) => {
                                            if (column.id === 'note') {
                                                this.props.setNote(rowInfo.row.note, rowInfo.row.id);
                                                this.simpleDialog.show();
                                            }
                                        }
                                    }
                                }}
                                columns={ Object.keys(patients.length === 0 ? patients : patients[0]).map(
                                    (attribute) => {
                                        return {
                                            Header: toTitleCase(attribute),
                                            accessor: attribute,
                                            Cell: row => <CellText cellValue={row.value} textQuery={textQuery} />
                                        };
                                    }
                                )}
                                defaultPageSize={10}
                                className="-striped -highlight" />
                        </div>
                    </div>
                    <SkyLight hideOnOverlayClicked ref={ref => this.simpleDialog =
                        ref} title={<h3>Patient Id: { this.props.displayedPatient }</h3>}>
                        Note: { this.props.displayedNote }
                    </SkyLight>
                    <br/>
                    <select onChange={this.handleChange} value={this.state.graphField} className="form-control">
                        { !filterParameters ? <option /> :
                            filterParameters.map((field, i) => {
                                return <option key={i} value={field.value}>{field.name}</option>;
                            })
                        }
                    </select>
                    &nbsp;
                    <button onClick={this.onAddGraph} className="btn">Add graph</button>
                    <br/><br/>
                    {graphs.map((graph, i) => {
                        return <Graph key={i} id={i} data={graph} />;
                    })}
                </div>
            );
        }
        else {
            return <div/>;
        }
    }
}

const mapStateToProps = state => {
    const { shouldDisplayResults, displayedNote, displayedPatient } = state.data;
    const { graphs } = state.graphs;
    const { textQuery } = state.query;
    const { filterParameters, fieldDisplay } = state.filter;

    const filteredEncounters = filterEncounters(state);

    return {
        shouldDisplayResults,
        patients: filterDisplayedFields(filteredEncounters, fieldDisplay),
        rawPatientData: filteredEncounters,
        displayedNote,
        displayedPatient,
        graphs,
        textQuery,
        filterParameters
    };
};

export default connect(mapStateToProps, { addGraph, setNote })(Results);
