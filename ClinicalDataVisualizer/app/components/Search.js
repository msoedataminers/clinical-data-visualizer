import React from 'react';
import { connect } from 'react-redux';

import { getData, editTextualSearch, fetchEncounters, clearGraphs } from '../actions/actions';

import QueryBuilder from './QueryBuilder';

//
// QueryBuilder
// Expected props:
// getData: func
// editTextualSearch: func
// clearGraphs: func
//
export class Search extends React.Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const { search, advancedSearch } = this.props;

        this.props.fetchEncounters({ search, advancedSearch });
        this.props.clearGraphs();
    }

    render() {
        const { search, advancedSearch, filter, editTextualSearch, addConditionalBlock, addRule, removeConditionalBlock,
            editRule, removeRule, fetchQueryParameters, fetchOperators } = this.props;

        return (
            <div>
                <h3 style={{ textAlign: 'center' }}>Clinical Data Visualizer</h3>
                <div className="col-sm-3 col-xs-1"></div>
                <div className="col-sm-6 col-xs-10">
                    <form onSubmit={this.handleSubmit}>
                        <div className="input-group">
                            <input className="form-control" type="text" value={search} onChange={ e => editTextualSearch(e.target.value) } autoFocus />
                            <span className="input-group-btn">
                                <input type="submit" className="btn" value="Search" />
                            </span>
                        </div>
                    </form>
                    <br />
                    <QueryBuilder />
                    <br />
                </div>
                <div className="col-sm-3 col-xs-1"></div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { textQuery, advancedSearch } = state.query;

    return {
        search: textQuery,
        advancedSearch
    };
};

export default connect(mapStateToProps, { getData, editTextualSearch, fetchEncounters, clearGraphs })(Search);
