import React from 'react';

import { calculateNumberOfPatients } from '../utils/utils';

//
// Results
// Expected props:
// patients: array
//
const ResultData = ({ patients }) => {
    const numberOfResults = patients.length;
    const numberOfPatients = calculateNumberOfPatients(patients);

    return (
        <h4 id='numResults'>{ numberOfResults } result{ numberOfResults === 1 ? "" : "s" } found { numberOfPatients < 0
            ? ", Patient numbers not found" : `across ${numberOfPatients} unique patient${ numberOfPatients === 1 ? "" : "s" }` }
        </h4>
    );
}

export default ResultData;
