import { createSelector } from 'reselect';

const getEncounters = state => state.data.patients;
const getFilters = state => state.filter.filters;

export const filterEncounters = createSelector(
    [getEncounters, getFilters],
    (encounters, filters) => {
        if (encounters && encounters.length) {
            let filteredEncounters = [ ...encounters ];
            for (let i in filters) {
                filteredEncounters = applyFilter(filteredEncounters, filters[i]);
            }
            return filteredEncounters;
        }
        return [];
    }
);

export const filterDisplayedFields = (encounters, displayedFields) =>
    encounters.map(_ => selectDisplayedFields(_, displayedFields));

const applyFilter = (encounters, filter) => {
    return encounters.filter(encounter =>
        applyComparison(encounter[filter.field], filter.value, filter.operator));
};

const applyComparison = (a, b, operator) => {
    switch(operator) {
        case '=':
            return a == b;
        case '>':
            return a > b;
        case '>=':
            return a >= b;
        case '<':
            return a < b;
        case '<=':
            return a <= b;
        case '<>':
            return a != b;
        default:
            return true;
    }
}

const selectDisplayedFields = (encounter, displayedFields) => {
    const displayableEncounter = {};

    Object.keys(encounter)
        .filter(_ => displayedFields[_])
        .forEach(_ => displayableEncounter[_] = encounter[_]);

    return displayableEncounter;
}
