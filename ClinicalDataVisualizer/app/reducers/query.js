import {
    EDIT_TEXTUAL_SEARCH,
    ADD_CONDITIONAL_BLOCK,
    REMOVE_CONDITIONAL_BLOCK,
    ADD_RULE,
    EDIT_RULE,
    REMOVE_RULE
} from '../actions/actions';


import { fields, operators } from '../components/QueryBuilder';

const initialState = {
    textQuery: '',
    advancedSearch: []
};

export default (state = initialState, action) => {
    const { textQuery, advancedSearch, ...newState } = state;

    switch (action.type) {
        case EDIT_TEXTUAL_SEARCH:
            return {
                textQuery: action.textQuery,
                advancedSearch,
                ...newState
            }
        case ADD_CONDITIONAL_BLOCK:
            return {
                textQuery,
                advancedSearch: [...advancedSearch, { rules: [] }],
                ...newState
            }
        case REMOVE_CONDITIONAL_BLOCK:
            return {
                textQuery,
                advancedSearch: removeBlock(advancedSearch, action.id),
                ...newState
            }
        case ADD_RULE:
            return {
                textQuery,
                advancedSearch: addRule(advancedSearch, action.id),
                ...newState
            }
        case EDIT_RULE:
            return {
                textQuery,
                advancedSearch: changeRule(advancedSearch, action.block, action.ruleId, action.rule),
                ...newState
            }
        case REMOVE_RULE:
            return {
                textQuery,
                advancedSearch: removeRule(advancedSearch, action.blockId, action.id),
                ...newState
            }
        default:
            return state;
    }
}

const removeBlock = (previousState, id) => {
    return previousState.filter((_, i) => i !== id);
}

const addRule = (previousState, id) => {
    return previousState.map((block, i) => {
        if (i === id)
            return {
                rules: [...block.rules, {
                    field: '',
                    operator: '',
                    value: '',
                    availableOperators: null
                }]
            };
        return block;
    });
}

const changeRule = (previousState, blockId, ruleId, newRule) => {
    return previousState.map((block, i) => {
        if (i === blockId) {
            const rules = block.rules.map((rule, j) => {
                if (j === ruleId) {
                    return newRule;
                }
                return rule;
            });
            return { rules };
        }
        return block;
    });
}

const removeRule = (previousState, blockId, ruleId) => {
    return previousState.map((block, i) => {
        if (i === blockId) {
            const rules = block.rules.filter((_, id) => id !== ruleId);
            return { rules };
        }
        return block;
    });
}
