﻿import {
    ADD_GRAPH,
    DELETE_GRAPH,
    CLEAR_GRAPHS
} from '../actions/actions';

const initialState = {
    graphs: []
};

export default (state = initialState, action) => {
    const { graphs, ...newState } = state;

    switch (action.type) {
        case ADD_GRAPH:
            return {
                graphs: [
                    ...graphs,
                    action.graphData
                ],
                ...newState
            }
        case DELETE_GRAPH:
            return {
                graphs: graphs.filter((data, id) => id !== action.id),
                ...newState
            }
        case CLEAR_GRAPHS:
            return {
                graphs: [],
                ...newState
            }
        default:
            return state;
    }
}