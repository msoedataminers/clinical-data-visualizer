import { combineReducers } from 'redux';

import data from './data';
import graphs from './graphs';
import query from './query';
import filter from './filter';

export default combineReducers({
    data,
    graphs,
    query,
    filter
});
