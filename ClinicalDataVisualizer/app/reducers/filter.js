import {
    ADD_FILTER,
    CLEAR_FILTERS,
    REMOVE_FILTER,
    EDIT_CURRENT_FILTER,
    GET_QUERY_PARAM_SUCCESS,
    GET_OPERATORS_SUCCESS,
    GET_FILTER_PARAM_SUCCESS,
    TOGGLE_FIELD_DISPLAY,
    SHOW_ALL_FIELDS,
    HIDE_ALL_FIELDS,
    GET_DATA_SUCCESS
} from '../actions/actions';

const initialState = {
    filters: [],
    queryParameters: null,
    operators: null,
    filterParameters: null,
    currentFilter: {
        field: '',
        operator: '',
        value: ''
    },
    fieldDisplay: {}
};

export default (state = initialState, action) => {
    const { filters, queryParameters, operators, filterParameters, currentFilter, fieldDisplay } = state;

    switch (action.type) {
        case ADD_FILTER:
            return {
                ...state,
                filters: [
                    ...filters,
                    currentFilter
                ]
            }
        case CLEAR_FILTERS:
            return {
                ...state,
                filters: []
            }
        case REMOVE_FILTER:
            return {
                ...state,
                filters: filters.filter((data, id) => id !== action.id)
            }
        case EDIT_CURRENT_FILTER:
            return {
                ...state,
                currentFilter: action.filter
            }
        case GET_QUERY_PARAM_SUCCESS:
            return {
                ...state,
                queryParameters: action.payload
            }
        case GET_OPERATORS_SUCCESS:
            return {
                ...state,
                operators: action.payload,
                currentFilter: {
                    field: currentFilter.field,
                    operator: action.payload[0].value,
                    value: currentFilter.value
                }
            }
        case GET_FILTER_PARAM_SUCCESS:
            return {
                ...state,
                filterParameters: action.payload,
                currentFilter: {
                    field: action.payload[0].value,
                    operator: currentFilter.operator,
                    value: currentFilter.value
                }
            }
        case GET_DATA_SUCCESS:
            return {
                ...state,
                fieldDisplay: initializeFieldsFromEncounters(action.payload)
            }
        case TOGGLE_FIELD_DISPLAY:
            return {
                ...state,
                fieldDisplay: {
                    ...fieldDisplay,
                    [action.field]: !fieldDisplay[action.field]
                }
            }
        case SHOW_ALL_FIELDS:
            return {
                ...state,
                fieldDisplay: setStateOfAllFields(fieldDisplay, true)
            }
        case HIDE_ALL_FIELDS:
            return {
                ...state,
                fieldDisplay: setStateOfAllFields(fieldDisplay, false)
            }
        default:
            return state;
    }
}

const initializeFieldsFromEncounters = encounters => {
    const fields = {};

    if (encounters && encounters.length) {
        Object.keys(encounters[0]).forEach(_ => fields[_] = true);
    }

    return fields;
}

const setStateOfAllFields = (fields, state) => {
    const newFields = {};

    Object.keys(fields).forEach(_ => newFields[_] = state);

    return newFields;
}
