import {
    GET_DATA_REQUEST,
    GET_DATA_SUCCESS,
    GET_DATA_FAILURE,
    SET_NOTE,
    REMOVE_NOTE
} from '../actions/actions';

const initialState = {
    shouldDisplayResults: false,
    patients: [],
    displayedNote: null,
    displayedPatient: null
};

export default (state = initialState, action) => {
    const { shouldDisplayResults, patients, displayedNote, displayedPatient,  ...newState } = state;

    switch (action.type) {
        case GET_DATA_REQUEST:
        case GET_DATA_FAILURE:
            return newState;
        case GET_DATA_SUCCESS:
            return {
                shouldDisplayResults: true,
                patients: action.payload,
                displayedNote: null,
                displayedPatient: null,
                ...newState
            }
        case SET_NOTE:
            return {
                shouldDisplayResults,
                patients,
                displayedNote: action.note,
                displayedPatient: action.patientId,
                ...newState
            }
        default:
            return state;
    }
}
