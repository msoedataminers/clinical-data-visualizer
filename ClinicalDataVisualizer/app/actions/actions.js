export const GET_DATA_REQUEST = 'clinical-data-visualizer/GET_DATA_REQUEST';
export const GET_DATA_SUCCESS = 'clinical-data-visualizer/GET_DATA_SUCCESS';
export const GET_DATA_FAILURE = 'clinical-data-visualizer/GET_DATA_FAILURE';

export const GET_QUERY_PARAM_REQUEST = 'clinical-data-visualizer/GET_QUERY_PARAM_REQUEST';
export const GET_QUERY_PARAM_SUCCESS = 'clinical-data-visualizer/GET_QUERY_PARAM_SUCCESS';
export const GET_QUERY_PARAM_FAILURE = 'clinical-data-visualizer/GET_QUERY_PARAM_FAILURE';

export const GET_OPERATORS_REQUEST = 'clinical-data-visualizer/GET_OPERATORS_REQUEST';
export const GET_OPERATORS_SUCCESS = 'clinical-data-visualizer/GET_OPERATORS_SUCCESS';
export const GET_OPERATORS_FAILURE = 'clinical-data-visualizer/GET_OPERATORS_FAILURE';

export const GET_FILTER_PARAM_REQUEST = 'clinical-data-visualizer/GET_FILTER_PARAM_REQUEST';
export const GET_FILTER_PARAM_SUCCESS = 'clinical-data-visualizer/GET_FILTER_PARAM_SUCCESS';
export const GET_FILTER_PARAM_FAILURE = 'clinical-data-visualizer/GET_FILTER_PARAM_FAILURE';

export const SET_NOTE = 'clinical-data-visualizer/SET_NOTE';

export const EDIT_TEXTUAL_SEARCH = 'clinical-data-visualizer/EDIT_TEXTUAL_SEARCH';

export const ADD_CONDITIONAL_BLOCK = 'clinical-data-visualizer/ADD_CONDITIONAL_BLOCK';
export const REMOVE_CONDITIONAL_BLOCK = 'clinical-data-visualizer/REMOVE_CONDITIONAL_BLOCK';

export const ADD_RULE = 'clinical-data-visualizer/ADD_RULE';
export const EDIT_RULE = 'clinical-data-visualizer/EDIT_RULE';
export const REMOVE_RULE = 'clinical-data-visualizer/REMOVE_RULE';

export const ADD_GRAPH = 'clinical-data-visualizer/ADD_GRAPH';
export const DELETE_GRAPH = 'clinical-data-visualizer/DELETE_GRAPH';
export const CLEAR_GRAPHS = 'clinical-data-visualizer/CLEAR_GRAPHS';

export const ADD_FILTER = 'clinical-data-visualizer/ADD_FILTER';
export const REMOVE_FILTER = 'clinical-data-visualizer/REMOVE_FILTER';
export const CLEAR_FILTERS = 'clinical-data-visualizer/CLEAR_FILTERS';
export const EDIT_CURRENT_FILTER = 'clinical-data-visualizer/EDIT_CURRENT_FILTER';

export const TOGGLE_FIELD_DISPLAY = 'clinical-data-visualizer/TOGGLE_FIELD_DISPLAY';
export const SHOW_ALL_FIELDS = 'clinical-data-visualizer/SHOW_ALL_FIELDS';
export const HIDE_ALL_FIELDS = 'clinical-data-visualizer/HIDE_ALL_FIELDS';

export const fetchEncounters = query => {
    return dispatch => {
        dispatch({ type: GET_DATA_REQUEST });

        const fetchConfig = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(query)
        };

        return fetch(`/api/encounter`, fetchConfig)
            .then(res => {
                if (!res.ok) throw new Error();
                return res.json();
            })
            .then(data => dispatch({ type: GET_DATA_SUCCESS, payload: data }))
            .catch(error => dispatch({ type: GET_DATA_FAILURE, payload: error }));
    }
};

export const fetchQueryParameters = () => {
    return makeGetRequest('/api/parameters/query', GET_QUERY_PARAM_REQUEST,
        GET_QUERY_PARAM_SUCCESS, GET_QUERY_PARAM_FAILURE);
};

export const fetchOperators = () => {
    return makeGetRequest('/api/parameters/operators', GET_OPERATORS_REQUEST,
        GET_OPERATORS_SUCCESS, GET_OPERATORS_FAILURE);
};

export const fetchFilterParameters = () => {
    return makeGetRequest('/api/parameters/filters', GET_FILTER_PARAM_REQUEST,
        GET_FILTER_PARAM_SUCCESS, GET_QUERY_PARAM_FAILURE);
};

const makeGetRequest = (url, request, success, failure) => {
    return dispatch => {
        dispatch({ type: request });

        return fetch(url)
            .then(res => {
                if (!res.ok) throw new Error();
                return res.json();
            })
            .then(data => dispatch({ type: success, payload: data }))
            .catch(error => dispatch({ type: failure, payload: error }));
    }
};

export const setNote = (note, patientId) => {
    return {
        type: SET_NOTE,
        note,
        patientId
    }
}

export const editTextualSearch = (textQuery) => {
    return {
        type: EDIT_TEXTUAL_SEARCH,
        textQuery
    }
};

export const addConditionalBlock = () => {
    return {
        type: ADD_CONDITIONAL_BLOCK
    }
};

export const removeConditionalBlock = (id) => {
    return {
        type: REMOVE_CONDITIONAL_BLOCK,
        id
    }
};

export const addRule = (id) => {
    return {
        type: ADD_RULE,
        id
    }
}

export const editRule = (block, ruleId, rule) => {
    return {
        type: EDIT_RULE,
        block,
        ruleId,
        rule
    }
}

export const removeRule = (blockId, id) => {
    return {
        type: REMOVE_RULE,
        blockId,
        id
    }
};

export const addGraph = (graphData) => {
    return {
        type: ADD_GRAPH,
        graphData
    }
};

export const deleteGraph = (id) => {
    return {
        type: DELETE_GRAPH,
        id
    }
};

export const clearGraphs = () => {
    return {
        type: CLEAR_GRAPHS
    }
};

export const addFilter = () => {
    return {
        type: ADD_FILTER
    }
};

export const removeFilter = (id) => {
    return {
        type: REMOVE_FILTER,
        id
    }
};

export const clearFilters = () => {
    return {
        type: CLEAR_FILTERS
    }
};

export const editCurrentFilter = filter => {
    return {
        type: EDIT_CURRENT_FILTER,
        filter
    }
};

export const toggleFieldDisplay = field => {
    return {
        type: TOGGLE_FIELD_DISPLAY,
        field
    }
};

export const showAllFields = () => {
    return {
        type: SHOW_ALL_FIELDS
    }
}

export const hideAllFields = () => {
    return {
        type: HIDE_ALL_FIELDS
    }
}
