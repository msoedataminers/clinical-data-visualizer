import 'jasmine';

import query from '../../reducers/query';

import {
    EDIT_TEXTUAL_SEARCH,
    ADD_CONDITIONAL_BLOCK,
    REMOVE_CONDITIONAL_BLOCK,
    ADD_RULE,
    EDIT_RULE,
    REMOVE_RULE
} from '../../actions/actions';

describe('query reducer', () => {

    let action, initialState;

    beforeEach(() => {
        action = {
            type: ''
        };

        initialState = {
            textQuery: '',
            advancedSearch: []
        };
    });

    it('should return original state for other action types', () => {
        action.type = 'INVALID_ACTION';

        const returnState = query(initialState, action);

        expect(returnState).toEqual(initialState);
    });

    describe('edit textual search action', () => {

        beforeEach(() => {
            action.type = EDIT_TEXTUAL_SEARCH;
            action.textQuery = 'hello world';
        });

        it('should edit the text query', () => {
            const returnState = query(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.textQuery).toEqual(action.textQuery);
            expect(returnState.advancedSearch).toEqual(initialState.advancedSearch);
        });

    });

    describe('add conditional block action', () => {

        beforeEach(() => {
            action.type = ADD_CONDITIONAL_BLOCK;
        });

        it('should add a conditional block', () => {
            const returnState = query(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.textQuery).toEqual(initialState.textQuery);
            expect(returnState.advancedSearch.length).toEqual(1);
        });

    });

    describe('remove conditional block action', () => {

        beforeEach(() => {
            action.type = REMOVE_CONDITIONAL_BLOCK;
            action.id = 0;

            initialState.advancedSearch = [{}];
        });

        it('should remove a conditional block', () => {
            const returnState = query(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.textQuery).toEqual(initialState.textQuery);
            expect(returnState.advancedSearch.length).toEqual(0);
        });

    });

    describe('add rule action', () => {

        beforeEach(() => {
            action.type = ADD_RULE;
            action.id = 0;

            initialState.advancedSearch = [{ rules: [] }, {}];
        });

        it('should add a rule', () => {
            const returnState = query(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.textQuery).toEqual(initialState.textQuery);
            expect(returnState.advancedSearch[0].rules.length).toEqual(1);
        });

    });

    describe('edit rule action', () => {

        beforeEach(() => {
            action.type = EDIT_RULE;
            action.block = 0;
            action.ruleId = 1;
            action.rule = {
                field: 'sex',
                operator: '<>',
                value: 'M'
            };

            initialState.advancedSearch = [{
                 rules: [{}, {
                     field: 'age',
                     operator: '=',
                     value: 21
                 }]
            }, {}];
        });

        it('should change the field of a rule', () => {
            const returnState = query(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.textQuery).toEqual(initialState.textQuery);
            expect(returnState.advancedSearch[0].rules[1]).toEqual(action.rule);
        });

    });

    describe('remove rule action', () => {

        beforeEach(() => {
            action.type = REMOVE_RULE;
            action.blockId = 0;
            action.id = 1;

            initialState.advancedSearch = [{ rules: [{}, {}] }, {}];
        });

        it('should remove a rule', () => {
            const returnState = query(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.textQuery).toEqual(initialState.textQuery);
            expect(returnState.advancedSearch[0].rules.length).toEqual(1);
        });

    });
});
