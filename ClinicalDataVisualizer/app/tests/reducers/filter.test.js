import 'jasmine';

import filter from '../../reducers/filter';

import {
    ADD_FILTER,
    CLEAR_FILTERS,
    REMOVE_FILTER,
    EDIT_CURRENT_FILTER,
    GET_QUERY_PARAM_SUCCESS,
    GET_OPERATORS_SUCCESS,
    GET_FILTER_PARAM_SUCCESS,
    TOGGLE_FIELD_DISPLAY,
    SHOW_ALL_FIELDS,
    HIDE_ALL_FIELDS,
    GET_DATA_SUCCESS,
    showAllFields
} from '../../actions/actions';

describe('filter reducer', () => {

    let action, initialState;

    beforeEach(() => {
        action = {
            type: ''
        };

        initialState = {
            filters: [],
            filterParameters: null,
            operators: null,
            filterParameters: null,
            currentFilter: {
                field: '',
                operator: '',
                value: ''
            },
            fieldDisplay: {}
        };
    });

    it('should return original state for other action types', () => {
        action.type = 'INVALID_ACTION';

        const returnState = filter(initialState, action);

        expect(returnState).toEqual(initialState);
    });

    describe('add filter action', () => {

        beforeEach(() => {
            action.type = ADD_FILTER;

            initialState.currentFilter = {
                field: 'age',
                operator: '>',
                value: 7
            };
        });

        it('adds the current filter to filters', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.filters[0]).toEqual(initialState.currentFilter);
        });

    });

    describe('clear filters action', () => {

        beforeEach(() => {
            action.type = CLEAR_FILTERS;

            initialState.filters = [{}, {}];
        });

        it('should clear the filters', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.filters.length).toEqual(0);
        });

    });

    describe('remove filter action', () => {

        beforeEach(() => {
            action.type = REMOVE_FILTER;
            action.id = 1;

            initialState.filters = [{}, {}];
        });

        it('should remove a filter', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.filters.length).toEqual(initialState.filters.length - 1);
        });

    });

    describe('edit current filter action', () => {

        beforeEach(() => {
            action.type = EDIT_CURRENT_FILTER;
            action.filter = {
                field: 'age',
                operator: '=',
                value: 18
            };
        });

        it('should edit the current filter', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.currentFilter).toEqual(action.filter);
        });

    });

    describe('get query params success action', () => {

        beforeEach(() => {
            action.type = GET_QUERY_PARAM_SUCCESS;
            action.payload = [{}, {}];
        });

        it('should set the filter parameters', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.currentFilter).toEqual(initialState.currentFilter);
            expect(returnState.queryParameters).toEqual(action.payload);
        });

    });

    describe('get operators success action', () => {

        beforeEach(() => {
            action.type = GET_OPERATORS_SUCCESS;
            action.payload = [{ value: '=' }, {}];
        });

        it('should set the operators and set the default operator', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.currentFilter.operator).toEqual(action.payload[0].value);
            expect(returnState.operators).toEqual(action.payload);
        });

    });

    describe('get filter params success action', () => {

        beforeEach(() => {
            action.type = GET_FILTER_PARAM_SUCCESS;
            action.payload = [{ value: 'age' }, {}];
        });

        it('should set the filter parameters', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.currentFilter.field).toEqual(action.payload[0].value);
            expect(returnState.filterParameters).toEqual(action.payload);
        });

    });

    describe('toggle field display action', () => {

        beforeEach(() => {
            initialState.fieldDisplay.age = true;

            action.type = TOGGLE_FIELD_DISPLAY;
            action.field = 'age';
        });

        it('should toggle the field display', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.fieldDisplay.age).toEqual(!initialState.fieldDisplay.age);
        });

    });

    describe('show all fields action', () => {

        beforeEach(() => {
            initialState.fieldDisplay.age = false;

            action.type = SHOW_ALL_FIELDS;
        });

        it('should show all fields', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.fieldDisplay.age).toBeTruthy();
        });

    });

    describe('hide all fields action', () => {

        beforeEach(() => {
            initialState.fieldDisplay.age = true;

            action.type = HIDE_ALL_FIELDS;
        });

        it('should hide all fields', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.fieldDisplay.age).toBeFalsy();
        });

    });

    describe('get data success action', () => {

        beforeEach(() => {
            action.type = GET_DATA_SUCCESS;
            action.payload = [{ age: 8, sex: 'F' }];
        });

        it('should initialize the field display', () => {
            const returnState = filter(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.fieldDisplay.age).toBeTruthy();
            expect(returnState.fieldDisplay.sex).toBeTruthy();
        });

        it('should initialize empty field display when no encounters', () => {
            action.payload = [];

            const returnState = filter(initialState, action);

            expect(returnState).toEqual(initialState);
        });
    });

});
