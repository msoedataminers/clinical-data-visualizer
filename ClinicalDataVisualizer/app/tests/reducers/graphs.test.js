import 'jasmine';

import graphs from '../../reducers/graphs';

import {
    ADD_GRAPH,
    DELETE_GRAPH,
    CLEAR_GRAPHS
} from '../../actions/actions';

describe('Graphs reducer', () => {

    let action, initialState;

    beforeEach(() => {
        action = {
            type: ''
        };

        initialState = {
            graphs: []
        };
    });

    it('should return original state for other action types', () => {
        action.type = 'INVALID_ACTION';

        const returnState = graphs(initialState, action);

        expect(returnState).toEqual(initialState);
    });

    describe('add graph action', () => {

        beforeEach(() => {
            action.type = ADD_GRAPH;
            action.graphData = {};
        });

        it('should add a graph', () => {
            let returnState = graphs(initialState, action);
            returnState = graphs(returnState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.graphs.length).toEqual(2);
        });

    });

    describe('delete graph action', () => {

        beforeEach(() => {
            action.type = DELETE_GRAPH;
            action.id = 1;

            initialState.graphs = [{}, {}];
        });

        it('should should remove the graph', () => {
            const returnState = graphs(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.graphs.length).toEqual(1);
        });

    });

    describe('clear graphs action', () => {

        beforeEach(() => {
            action.type = CLEAR_GRAPHS;

            initialState.graphs = [{}, {}];
        });

        it('should clear the graphs', () => {
            const returnState = graphs(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.graphs.length).toEqual(0);
        });;

    });

});
