import 'jasmine';

import data from '../../reducers/data';

import {
    GET_DATA_REQUEST,
    GET_DATA_SUCCESS,
    GET_DATA_FAILURE,
    SET_NOTE
} from '../../actions/actions';

describe('Data reducer', () => {

    let action, initialState;

    beforeEach(() => {
        action = {
            type: '',
            payload: {}
        };

        initialState = {
            shouldDisplayResults: false,
            patients: [],
            displayedNote: null,
            displayedPatient: null
        };
    });

    it('should return original state for other action types', () => {
        action.type = 'INVALID_ACTION';

        const returnState = data(initialState, action);

        expect(returnState).toEqual(initialState);
    });

    describe('get data success action', () => {

        beforeEach(() => {
            action.type = GET_DATA_SUCCESS;
            action.payload = [{}, {}, {}];
        });

        it('should return object with patients', () => {
            const returnState = data(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.shouldDisplayResults).toBe(true);
            expect(returnState.patients).toEqual(action.payload);
        });

    });

    describe('get data request action', () => {

        beforeEach(() => {
            action.type = GET_DATA_REQUEST;
        });

        it('should return rest of the state', () => {
            const returnState = data(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.shouldDisplayResults).toBe(undefined);
            expect(returnState.patients).toBe(undefined);
        });

    });

    describe('get data failure action', () => {

        beforeEach(() => {
            action.type = GET_DATA_FAILURE;
        });

        it('should return rest of the state', () => {
            const returnState = data(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.shouldDisplayResults).toBeUndefined();
            expect(returnState.patients).toBe(undefined);
        });;

    });

    describe('set note action', () => {

        beforeEach(() => {
            action.type = SET_NOTE;
            action.note = 'Hello world';
            action.patientId = 'Someone';
        });

        it('should return rest of the state', () => {
            const returnState = data(initialState, action);

            expect(returnState).not.toEqual(initialState);
            expect(returnState.displayedNote).toEqual(action.note);
            expect(returnState.displayedPatient).toEqual(action.patientId);
        });;

    });

});
