import 'jasmine';

import { filterDisplayedFields } from '../../selectors';

describe('filterDisplayedFields selector', () => {

    let encounters, fieldDisplay;

    beforeEach(() => {
        encounters = [];
        fieldDisplay = {};
    });

    it('should only display specified fields', () => {
        encounters.push({ age: 18, sex: 'M' });
        fieldDisplay.age = true;

        const selectedState = filterDisplayedFields(encounters, fieldDisplay);

        expect(selectedState[0]).toEqual({ age: 18 });
    });

    it('should not display fields if none are specified', () => {
        encounters.push({ age: 18, sex: 'M' });

        const selectedState = filterDisplayedFields(encounters, fieldDisplay);

        expect(selectedState[0]).toEqual({});
    });

    it('should display fields if all are specified', () => {
        encounters.push({ age: 18, sex: 'M' });
        fieldDisplay.age = true;
        fieldDisplay.sex = true;

        const selectedState = filterDisplayedFields(encounters, fieldDisplay);

        expect(selectedState[0]).toEqual(encounters[0]);
    });

});
