import 'jasmine';

import { filterEncounters } from '../../selectors';

describe('filterEncounters selector', () => {

    let state;

    beforeEach(() => {
        state = {
            data: {
                patients: []
            },
            filter: {
                filters: [],
            }
        }
    });

    it('should return an empty list of patients when patients are null', () => {
        state.data.patients = null;

        const selectedState = filterEncounters(state);

        expect(selectedState).toEqual([]);
    });

    it('should return the original list of patients with no filters', () => {
        state.data.patients = [{}, {}];

        const selectedState = filterEncounters(state);

        expect(selectedState).toEqual(state.data.patients);
    });

    it('should return a filtered list of patients with filters', () => {
        state.data.patients = [{ age: 18 }, { age: 21 }];
        state.filter.filters.push({ field: 'age', operator: '>', value: '20' });
        state.filter.filters.push({ field: 'age', operator: '>=', value: '20' });
        state.filter.filters.push({ field: 'age', operator: '<', value: '22' });
        state.filter.filters.push({ field: 'age', operator: '<=', value: '22' });

        const selectedState = filterEncounters(state);

        expect(selectedState).toEqual([ state.data.patients[1] ]);
    });

    it('should return an unfiltered list of patients with filters that do not meet criteria', () => {
        state.data.patients = [{ age: 18 }, { age: 21 }];
        state.filter.filters.push({ field: 'age', operator: '<>', value: '22' });
        state.filter.filters.push({ field: 'age', operator: 'might be', value: '22' });

        const selectedState = filterEncounters(state);

        expect(selectedState).toEqual(state.data.patients);
    });

    it('should return the no patients with encompassing filters', () => {
        state.data.patients = [{ age: 18 }, { age: 21 }];
        state.filter.filters = [{ field: 'age', operator: '=', value: '30' }];

        const selectedState = filterEncounters(state);

        expect(selectedState.length).toEqual(0);
    });

});
