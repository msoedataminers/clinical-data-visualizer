import 'jasmine';
import 'core-js';

import React from 'react';
import { renderIntoDocument } from 'react-dom/test-utils';

import { Graph } from '../../components/Graph';

describe('Graph', () => {
    it('should render into DOM', () => {
        const element = <Graph id={1} data={[]} />;

        expect(() => {
            renderIntoDocument(element);
        }).not.toThrow();
    });
});
