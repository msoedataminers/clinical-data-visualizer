import 'jasmine';
import 'core-js';

import React from 'react';
import { renderIntoDocument } from 'react-dom/test-utils';

import SelectList from '../../components/SelectList';

describe('SelectList', () => {
    it('should render into DOM', () => {
        const element = <SelectList
            options={[{ name: 'Hello', value: 'hello'}]}
            value='hello' />;

        expect(() => {
            renderIntoDocument(element);
        }).not.toThrow();
    });
});
