import 'jasmine';
import 'core-js';

import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { renderIntoDocument } from 'react-dom/test-utils';

import Rule from '../../components/Rule';

describe('Rule', () => {
    let mockStore, store;

    beforeEach(() => {
        mockStore = configureMockStore();
    });

    it('should render into DOM',
        () => {
            store = mockStore({});

            const element = <Provider store={store}>
                    <Rule id={0}
                        blockId={0}
                        rule={{ field: "field", operator: "=", value: "value" }}
                        fields={[{ name: "Field", value: "field" }]}
                        operators={[{ name: "=", value: "=" }]}/>
                </Provider>;

            expect(() => {
                renderIntoDocument(element);
            }).not.toThrow();
        });
    });
