import 'jasmine';
import 'core-js';

import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { renderIntoDocument } from 'react-dom/test-utils';

import { Results } from '../../components/Results';

describe('Results', () => {
    let mockStore, store;

    beforeEach(() => {
        mockStore = configureMockStore();
    });

    it('should render into DOM',
        () => {
            store = mockStore({
                query: { advancedSearch: [] },
                filter: { queryParameters: [{}], operators: [{}] }
            });

            const element = <Provider store={store}>
                    <Results />
                </Provider>;

            expect(() => {
                renderIntoDocument(element);
            }).not.toThrow();
        });
    });
