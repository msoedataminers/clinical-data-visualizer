import 'jasmine';
import 'core-js';

import React from 'react';
import { renderIntoDocument } from 'react-dom/test-utils';

import TypeAheadList from '../../components/TypeAheadList';

describe('SelectList', () => {
    it('should render into DOM', () => {
        const element = <TypeAheadList
            id='test'
            options={[{ name: 'Hello' }]}
            selectedValue='hello' />;

        expect(() => {
            renderIntoDocument(element);
        }).not.toThrow();
    });
});
