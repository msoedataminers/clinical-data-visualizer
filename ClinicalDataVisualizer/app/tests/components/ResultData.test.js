import 'jasmine';
import 'core-js';

import React from 'react';
import { renderIntoDocument } from 'react-dom/test-utils';
import { findDOMNode, render } from 'react-dom';

import ResultData from '../../components/ResultData';

describe('ResultData', () => {
    it('should render into DOM', () => {
        const element = <ResultData patients={[ { patientNumber: 123456}, { patientNumber: 123456} ]} />;

        expect(() => {
            renderIntoDocument(element);
        }).not.toThrow();
    });

    it('should specify no patient ids', () => {
        const element = <ResultData patients={[ {}, {} ]} />;

        expect(() => {
            renderIntoDocument(element);
        }).not.toThrow();
    });
});
