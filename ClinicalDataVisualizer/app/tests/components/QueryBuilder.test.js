import 'jasmine';
import 'core-js';

import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { renderIntoDocument } from 'react-dom/test-utils';

import QueryBuilder from '../../components/QueryBuilder';

describe('QueryBuilder', () => {
    let mockStore, store;

    beforeEach(() => {
        mockStore = configureMockStore();
    });

    it('should render into DOM',
        () => {
            store = mockStore({
                query: { advancedSearch: [] },
                filter: { queryParameters: [{}], operators: [{}] }
            });

            const element = <Provider store={store}>
                                <QueryBuilder />
                            </Provider>;

            expect(() => {
                renderIntoDocument(element);
            }).not.toThrow();
        });
    });
