import 'jasmine';
import 'core-js';

import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { renderIntoDocument } from 'react-dom/test-utils';

import FieldSelector from '../../components/FieldSelector';

describe('FieldSelector', () => {

    let mockStore, store;

    beforeEach(() => {
        mockStore = configureMockStore();
    });

    it('should render into DOM', () => {
        store = mockStore({ filter: { fieldDisplay: { age: true, sex: false } } });

        const element = <Provider store={store}>
                <FieldSelector />
            </Provider>;

        expect(() => {
            renderIntoDocument(element);
        }).not.toThrow();
    });
});
