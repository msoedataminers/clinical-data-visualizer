import 'jasmine';
import 'core-js';

import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { renderIntoDocument } from 'react-dom/test-utils';

import ConditionalBlock from '../../components/ConditionalBlock';

describe('ConditionalBlock', () => {

    let mockStore, store;

    beforeEach(() => {
        mockStore = configureMockStore();
    });

    it('should render into DOM',
        () => {
            store = mockStore({});

            const element = <Provider store={store}>
                    <ConditionalBlock
                        id={0}
                        rules={[{ field: "field", operator: "=", value: "value" }]}
                        fields={[{ name: "Field", value: "field" }]}
                        operators={[{ name: "=", value: "=" }]} />
                </Provider>;

            expect(() => {
                renderIntoDocument(element);
            }).not.toThrow();
        });
    });
