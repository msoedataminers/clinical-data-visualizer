import 'jasmine';
import 'core-js';

import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { renderIntoDocument } from 'react-dom/test-utils';

import Search from '../../components/Search';

describe('Search', () => {

    let mockStore, store;

    beforeEach(() => {
        mockStore = configureMockStore();
    });

    it('should render into DOM',
        () => {
            store = mockStore({
                query: { textQuery: 'Hello world', advancedSearch: [] },
                filter: { queryParameters: [], operators: [] }
            });

            const element = <Provider store={store}>
                                <Search />
                            </Provider>;

            expect(() => {
                renderIntoDocument(element);
            }).not.toThrow();
        });
    });
