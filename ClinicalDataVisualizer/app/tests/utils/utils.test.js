import 'jasmine';
import 'core-js';

import React from 'react';
import { connect } from 'react-redux';

import { createGraph, toTitleCase, formatCellText, checkSearchTermEquality, calculateNumberOfPatients } from '../../utils/utils';

describe('Utils',
    () => {
        it('should create a simple graph of gender',
            () => {
                expect(createGraph([{ sex: 'f' }, { sex: 'm' }], 'sex').sort((item1, item2) => {
                    return item1.name.localeCompare(item2.name);
                })).toEqual([{ name: 'f', value: 1 }, { name: 'm', value: 1 }]);
            });

        it('should create a simple graph of age',
            () => {
                expect(createGraph([{ age: 12 }, { age: 24 }, { age: 72 }, { age: 96 }, { age: 1 }, { age: 24 }], 'age').sort((item1, item2) => {
                    return item1.name.localeCompare(item2.name);
                })).toEqual([{ name: '1', value: 1 }, { name: '12', value: 1 }, { name: '24', value: 2 }, { name: '72', value: 1 }, { name: '96', value: 1 }]);
            });

        it('should capitalize a single word',
            () => {
                expect(toTitleCase("simple")).toEqual("Simple");
            });

        it('should title case two words',
            () => {
                expect(toTitleCase("titlePhrase")).toEqual("Title Phrase");
            });

        it('should title case many words',
            () => {
                expect(toTitleCase("insertManyWordsHere")).toEqual("Insert Many Words Here");
            });

        it('should assure a string equals itself',
            () => {
                expect(checkSearchTermEquality("This is a sentence of many words.", "This is a sentence of many words.")).toBe(true);
            });

        it('should assure the empty string equals itself',
            () => {
                expect(checkSearchTermEquality("", "")).toBe(true);
            });

        it('should assure strings are equal ignoring case',
            () => {
                expect(checkSearchTermEquality("AwKwArD cApItAlIzAtIoN!", "aWkWaRd CaPiTaLiZaTiOn!")).toBe(true);
            });

        it('should assure different strings are not equal ignoring case',
            () => {
                expect(checkSearchTermEquality("This STRING is different FROM the next.", "This STRING is different FROM the previous.")).toBe(false);
            });

        it('should calculate the number of patients', () => {
            const patients = [{ patientNumber: 1 }, { patientNumber: 1 }];

            expect(calculateNumberOfPatients(patients)).toEqual(1);
        });

        it('should calculate -1 when no patient numbers are present', () => {
            const patients = [{ id: 1 }, { id: 2 }];

            expect(calculateNumberOfPatients(patients)).toEqual(-1);
        });
    });
