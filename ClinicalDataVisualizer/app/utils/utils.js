export const createGraph = (data, field) => {

    const result = [];
    const results = {};

    for (let i in data) {
        if (results[data[i][field]] == undefined) {
            results[data[i][field]] = 1;
        }
        else {
            results[data[i][field]]++;
        }
    }

    Object.keys(results).map(key => {
        result.push({ name: key, value: results[key] });
    });

    result.sort();

    return result;
};

export const toTitleCase = (text) => {
    return text.charAt(0).toUpperCase() +
        text.replace(/([A-Z]+)*([A-Z][a-z])/g, "$1 $2").substr(1);
}

export const checkSearchTermEquality = (resultWord, queryWord) => {
    return resultWord.toUpperCase() === queryWord.toUpperCase();
}

export const calculateNumberOfPatients = patients => {
    const patientIds = {};
    for (let i in patients) {
        const id = patients[i].patientNumber;
        if (id == null) return -1;

        if (patientIds[id]) patientIds[id]++;
        else patientIds[id] = 1;
    }
    return Object.keys(patientIds).length;
}
