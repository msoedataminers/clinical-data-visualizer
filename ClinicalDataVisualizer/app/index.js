import 'whatwg-fetch';
import 'core-js';

import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';

import './style.css'

import App from './components/App';

import reducers from './reducers';

const middlewares = [thunk];

if (process.env.NODE_ENV !== 'production') {
    middlewares.push(createLogger());
}

const store = createStore(reducers, applyMiddleware(...middlewares));

render(
    <Provider store={store}>
        <App />
    </Provider>
, document.getElementById('app'));
