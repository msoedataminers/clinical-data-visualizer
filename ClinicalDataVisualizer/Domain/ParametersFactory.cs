using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ClinicalDataVisualizer.DAL;
using ClinicalDataVisualizer.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace ClinicalDataVisualizer.Domain
{
    public interface IParametersFactory
    {
        IList<QueryParameter> CreateQueryParameters();
        IList<Parameter> CreateOperators();
        IList<Parameter> CreateFilterParameters();
    }

    public class ParametersFactory : IParametersFactory
    {
        private readonly ConfigurationProxy _configurationProxy;
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public ParametersFactory(IOptions<ConfigurationProxy> configurationProxy, DataContext context, IMapper mapper)
        {
            _configurationProxy = configurationProxy.Value ?? throw new ArgumentNullException(nameof(configurationProxy));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public IList<QueryParameter> CreateQueryParameters()
        {
            var queryEntities = _context.QueryParameters
                .Include(query => query.Type)
                    .ThenInclude(_ => _.Operators)
                        .ThenInclude(_ => _.Operator);

            var queryParameters = queryEntities.Select(_mapper.Map<QueryParameter>).ToList();

            return queryParameters;
        }

        public IList<Parameter> CreateOperators()
        {
            var operatorValues = _configurationProxy.Operators;

            var operators = operatorValues.Select(_ => new Parameter { Name = _, Value = _ }).ToList();

            return operators;
        }

        public IList<Parameter> CreateFilterParameters()
        {
            var filterParameterValues = _configurationProxy.FilterParameters;

            var filterParameters = filterParameterValues.Select(_ => new Parameter { Name = _, Value = LowerFirstCharacter(_) }).ToList();

            return filterParameters;
        }

        private static string LowerFirstCharacter(string word)
        {
            return char.ToLower(word[0]) + word.Substring(1);
        }
    }
}