using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ClinicalDataVisualizer.DAL;
using ClinicalDataVisualizer.Domain.Models;

namespace ClinicalDataVisualizer.Domain
{
    public interface IEncounterFactory
    {
        IEnumerable<Encounter> FetchEncounters(EncounterRequest query);
    }

    public class EncounterFactory : IEncounterFactory
    {
        private readonly IPatientClientProxy _patientClientProxy;
        private readonly IMapper _mapper;

        public EncounterFactory(IPatientClientProxy patientClientProxy, IMapper mapper)
        {
            _patientClientProxy = patientClientProxy ?? throw new ArgumentNullException(nameof(patientClientProxy));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public IEnumerable<Encounter> FetchEncounters(EncounterRequest query)
        {
            if (query == null)
                throw new ArgumentNullException(nameof(query));

            var patientServiceResponse = _patientClientProxy.MakePatientCall(query);

            var encounters = patientServiceResponse.Response?.Docs?.Select(_mapper.Map<Encounter>);

            return encounters;
        }
    }
}
