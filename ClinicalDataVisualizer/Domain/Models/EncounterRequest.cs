using System.Collections.Generic;

namespace ClinicalDataVisualizer.Domain.Models
{
    public class EncounterRequest
    {
        public string Search { get; set; }
        public IEnumerable<ConditionalBlock> AdvancedSearch { get; set; }
    }
}
