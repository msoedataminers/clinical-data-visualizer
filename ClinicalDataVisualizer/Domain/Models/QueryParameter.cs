using System.Collections.Generic;

namespace ClinicalDataVisualizer.Domain.Models
{
    public class QueryParameter
    {
        public string Query { get; set; }
        public string Type { get; set; }
        public IList<string> Operators { get; set; }
    }
}