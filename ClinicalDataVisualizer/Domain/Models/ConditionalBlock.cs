using System.Collections.Generic;

namespace ClinicalDataVisualizer.Domain.Models
{
    public class ConditionalBlock
    {
        public List<Rule> Rules { get; set; }
    }
}