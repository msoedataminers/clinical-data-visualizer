﻿using System;
using System.Collections.Generic;

namespace ClinicalDataVisualizer.Domain.Models
{
    public class Encounter
    {
        public string Id { get; set; }
        public long EncounterNumber { get; set; }
        public long PatientNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime BirthDate { get; set; }
        public string Vitals { get; set; }
        public string Sex { get; set; }
        public int Age { get; set; }
        public string AgeCategory { get; set; }
        public string Race { get; set; }
        public string MaritalStatus { get; set; }
        public string Zip { get; set; }
        public string Note { get; set; }
        public IList<string> Icd9Codes { get; set; }
    }
}