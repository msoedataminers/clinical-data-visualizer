using System;
using ClinicalDataVisualizer.DAL.Models;
using ClinicalDataVisualizer.Domain.Models;
using RestSharp;

namespace ClinicalDataVisualizer.DAL
{
    public interface IPatientClientProxy
    {
        PatientServiceResponse MakePatientCall(EncounterRequest request);
    }

    public class PatientClientProxy : IPatientClientProxy
    {
        private readonly IRestClientWrapper _restClientWrapper;

        public PatientClientProxy(IRestClientWrapper restClientWrapper)
        {
            _restClientWrapper = restClientWrapper ?? throw new ArgumentNullException(nameof(restClientWrapper));
        }

        public PatientServiceResponse MakePatientCall(EncounterRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var encounterRequest = new RestRequest
            {
                Resource = BuildRequestResource(request),
                RequestFormat = DataFormat.Json
            };

            var patientServiceResponse = _restClientWrapper.PatientCall(encounterRequest);

            return patientServiceResponse;
        }

        private static string BuildRequestResource(EncounterRequest request)
        {
            return string.Empty;
        }
    }
}
