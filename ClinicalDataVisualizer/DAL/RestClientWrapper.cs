using System;
using ClinicalDataVisualizer.DAL.Models;
using RestSharp;
using RestSharp.Deserializers;

namespace ClinicalDataVisualizer.DAL
{
    public interface IRestClientWrapper
    {
        IRestClient PatientClient { get; set; }
        IDeserializer Deserializer { get; set; }
        PatientServiceResponse PatientCall(IRestRequest request);
    }

    public class RestClientWrapper : IRestClientWrapper
    {
        public IRestClient PatientClient { get; set; }
        public IDeserializer Deserializer { get; set; }

        public RestClientWrapper()
        {
            PatientClient = new RestClient
            {
                BaseUrl = new Uri(Constants.PatientDataBaseUri)
            };
            Deserializer = new JsonDeserializer();
        }

        public virtual PatientServiceResponse PatientCall(IRestRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var response = PatientClient.Execute(request);

            var patientServiceResponse = Deserializer.Deserialize<PatientServiceResponse>(response);

            return patientServiceResponse;
        }
    }
}
