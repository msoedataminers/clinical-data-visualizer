using System.Collections.Generic;

namespace ClinicalDataVisualizer.DAL.Models
{
    public class TypeEntity
    {
        public int Id { get; set; }
        public string Type { get; set; }

        public IList<QueryEntity> Queries { get; set; }
        public IList<TypeOperatorEntity> Operators { get; set; }
    }
}