using System.Collections.Generic;

namespace ClinicalDataVisualizer.DAL.Models
{
    public class OperatorEntity
    {
        public int Id { get; set; }
        public string Operator { get; set; }

        public IList<TypeOperatorEntity> Types { get; set; }
    }
}