namespace ClinicalDataVisualizer.DAL.Models
{
    public class PatientServiceResponse
    {
        public ResponseDetails Response { get; set; }
    }
}