using System.Collections.Generic;

namespace ClinicalDataVisualizer.DAL.Models
{
    public class TypeOperatorEntity
    {
        public int TypeId { get; set; }
        public int OperatorId { get; set; }

        public TypeEntity Type { get; set; }
        public OperatorEntity Operator { get; set; }
    }
}