using System;
using System.Collections.Generic;
using RestSharp.Deserializers;

namespace ClinicalDataVisualizer.DAL.Models
{
    public class Doc
    {
        public string Id { get; set; }
        [DeserializeAs(Name = "encounter_num")]
        public string EncounterNumber { get; set; }
        [DeserializeAs(Name = "patient_num")]
        public string PatientNumber { get; set; }
        [DeserializeAs(Name = "start_date")]
        public DateTime StartDate { get; set; }
        [DeserializeAs(Name = "end_date")]
        public List<DateTime> EndTime { get; set; }
        [DeserializeAs(Name = "birth_date")]
        public DateTime BirthDate { get; set; }
        [DeserializeAs(Name = "vital_status")]
        public string VitalStatus { get; set; }
        public string Sex { get; set; }
        public int Age { get; set; }
        public string Race { get; set; }
        [DeserializeAs(Name = "marital_status")]
        public string MaritalStatus { get; set; }
        public string Zip { get; set; }
        [DeserializeAs(Name = "note_text")]
        public List<string> Note { get; set; }
        public List<string> Icd9s { get; set; }
        [DeserializeAs(Name = "age_cat")]
        public string AgeCategory { get; set; }
    }
}
