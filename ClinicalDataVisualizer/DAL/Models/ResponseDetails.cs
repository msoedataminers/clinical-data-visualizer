using System.Collections.Generic;

namespace ClinicalDataVisualizer.DAL.Models
{
    public class ResponseDetails
    {
        public int NumFound { get; set; }
        public int Start { get; set; }
        public List<Doc> Docs { get; set; }
    }
}
