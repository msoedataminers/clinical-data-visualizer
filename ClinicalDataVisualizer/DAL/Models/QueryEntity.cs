using System.Collections.Generic;

namespace ClinicalDataVisualizer.DAL.Models
{
    public class QueryEntity
    {
        public int Id { get; set; }
        public string Query { get; set; }
        public int TypeId { get; set; }

        public TypeEntity Type { get; set; }
    }
}