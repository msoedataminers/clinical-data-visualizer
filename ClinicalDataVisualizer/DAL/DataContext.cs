using ClinicalDataVisualizer.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace ClinicalDataVisualizer.DAL
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<QueryEntity> QueryParameters { get; set; }
        public DbSet<TypeEntity> Types { get; set; }
        public DbSet<TypeOperatorEntity> TypeOperators { get; set; }
        public DbSet<OperatorEntity> Operators { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<QueryEntity>(entity =>
            {
                entity.ToTable("queries").HasKey(_ => _.Id);

                entity.Property(_ => _.Id)
                    .HasColumnName("id");

                entity.Property(_ => _.Query)
                    .HasColumnName("query");

                entity.Property(_ => _.TypeId)
                    .HasColumnName("typeid");

                entity.HasOne(_ => _.Type)
                    .WithMany(_ => _.Queries)
                    .HasForeignKey(_ => _.TypeId);
            });

            modelBuilder.Entity<TypeEntity>(entity =>
            {
                entity.ToTable("type").HasKey(_ => _.Id);

                entity.Property(_ => _.Id)
                    .HasColumnName("typeid");

                entity.Property(_ => _.Type)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<OperatorEntity>(entity =>
            {
                entity.ToTable("operator").HasKey(_ => _.Id);

                entity.Property(_ => _.Id)
                    .HasColumnName("operatorid");

                entity.Property(_ => _.Operator)
                    .HasColumnName("operator");
            });

            modelBuilder.Entity<TypeOperatorEntity>(entity =>
            {
                entity.ToTable("typeoperator").HasKey(_ => new { _.TypeId, _.OperatorId });

                entity.Property(_ => _.TypeId)
                    .HasColumnName("typeid");

                entity.Property(_ => _.OperatorId)
                    .HasColumnName("operatorid");

                entity.HasOne(_ => _.Type)
                    .WithMany(_ => _.Operators)
                    .HasForeignKey(_ => _.TypeId);

                entity.HasOne(_ => _.Operator)
                    .WithMany(_ => _.Types)
                    .HasForeignKey(_ => _.OperatorId);
            });
        }
    }
}