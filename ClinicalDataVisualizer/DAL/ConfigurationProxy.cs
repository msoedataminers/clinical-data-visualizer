using System.Collections.Generic;

namespace ClinicalDataVisualizer.DAL
{
    public class ConfigurationProxy
    {
        public IList<string> Operators { get; set; }
        public IList<string> FilterParameters { get; set; }
    }
}