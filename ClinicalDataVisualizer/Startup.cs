using System;
using System.Collections.Generic;
using AutoMapper;
using ClinicalDataVisualizer.Automapper;
using ClinicalDataVisualizer.DAL;
using ClinicalDataVisualizer.DAL.Models;
using ClinicalDataVisualizer.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using RestSharp;

namespace ClinicalDataVisualizer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ConfigurationProxy>(Configuration);

            services.AddMvc();

            services.AddDbContext<DataContext>(options => options.UseSqlite(Configuration.GetConnectionString("Sqlite")));

            services.AddTransient<IMapper, Mapper>(ctx => AutomapperFactory.CreateMapper());
            services.AddTransient<IEncounterFactory, EncounterFactory>();
            services.AddTransient<IParametersFactory, ParametersFactory>();
            services.AddTransient<IPatientClientProxy, PatientClientProxy>();
            services.AddTransient<IRestClientWrapper, RestClientWrapper>(ctx => MockClientData().Object);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
            optionsBuilder.UseSqlite(Configuration.GetConnectionString("Sqlite"));

            using (var context = new DataContext(optionsBuilder.Options))
            {
                context.Database.EnsureCreated();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();
        }

        private Mock<RestClientWrapper> MockClientData()
        {
            var mock = new Mock<RestClientWrapper>();
            mock.Setup(_ => _.PatientCall(It.IsAny<IRestRequest>())).Returns(new PatientServiceResponse
            {
                Response = new ResponseDetails
                {
                    NumFound = 3,
                    Start = 0,
                    Docs = new List<Doc>
                    {
                        new Doc
                        {
                            Id = "Patient1",
                            EncounterNumber = "1",
                            PatientNumber = "1001",
                            StartDate = new DateTime(2016, 5, 3),
                            EndTime = new List<DateTime>
                            {
                                new DateTime(2016, 6, 2),
                                new DateTime(2016, 6, 3)
                            },
                            BirthDate = new DateTime(1985, 1, 3),
                            VitalStatus = "C",
                            Sex = "F",
                            Age = 39,
                            Race = "Caucasian",
                            MaritalStatus = "M",
                            Zip = "53202",
                            Note = new List<string>
                            {
                                "text1",
                                "text2"
                            },
                            Icd9s = new List<string>
                            {
                                "j3ki",
                                "dd0"
                            },
                            AgeCategory = "30-39"
                        },
                        new Doc
                        {
                            Id = "Patient2",
                            EncounterNumber = "13",
                            PatientNumber = "1011",
                            StartDate = new DateTime(2016, 5, 6),
                            EndTime = new List<DateTime>
                            {
                                new DateTime(2016, 5, 20),
                                new DateTime(2016, 6, 1)
                            },
                            BirthDate = new DateTime(2003, 10, 17),
                            VitalStatus = "N",
                            Sex = "M",
                            Age = 22,
                            Race = "African American",
                            MaritalStatus = "S",
                            Zip = "53206",
                            Note = new List<string>
                            {
                                "text1",
                                "text2"
                            },
                            Icd9s = new List<string>
                            {
                                "io3p0"
                            },
                            AgeCategory = "20-29"
                        },
                        new Doc
                        {
                            Id = "Patient3",
                            EncounterNumber = "45",
                            PatientNumber = "2246",
                            StartDate = new DateTime(2016, 2, 1),
                            EndTime = new List<DateTime>
                            {
                                new DateTime(2016, 2, 2),
                                new DateTime(2016, 2, 27)
                            },
                            BirthDate = new DateTime(1994, 7, 12),
                            VitalStatus = "O",
                            Sex = "F",
                            Age = 25,
                            Race = "Asian",
                            MaritalStatus = "M",
                            Zip = "53202",
                            Note = new List<string>
                            {
                                "text1",
                                "text2"
                            },
                            Icd9s = new List<string>
                            {
                                "2m8n",
                                "q0p9",
                                "666"
                            },
                            AgeCategory = "20-29"
                        }
                    }
                }
            });
            return mock;
        }
    }
}