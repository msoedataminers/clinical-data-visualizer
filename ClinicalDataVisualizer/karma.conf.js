const webpackConfig = require('./webpack.config');

webpackConfig.module.loaders[0].query.plugins.push(['istanbul', { 'exclude': ['app/tests/**'] }]);

module.exports = config => {

    config.set({

        browsers: ['PhantomJS'],

        singleRun: true,

        frameworks: ['jasmine'],

        files: ['app/tests/**/*.test.js'],

        reporters: ['progress', 'coverage'],

        coverageReporter: {
            type: 'text'
        },

        preprocessors: {
            'app/tests/**/*.test.js': ['webpack', 'sourcemap']
        },

        webpack: {
            module: webpackConfig.module,
            resolve: webpackConfig.resolve,
            plugins: webpackConfig.plugins,
            node: {
                fs: 'empty'
            }
        },

        webpackServer: {
            noInfo: true
        }

    });

}
